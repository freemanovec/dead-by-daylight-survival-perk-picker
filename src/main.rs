use rand::seq::SliceRandom;

fn main() {
    let perks = include_str!("../perks.txt").split('\n').map(|s| String::from(s)).collect::<Vec<String>>();

    let mut rng = rand::thread_rng();

    let selected_perks = perks.choose_multiple(&mut rng, 4).map(|s| String::from(s)).collect::<Vec<String>>();

    let mut ix = 0;
    for perk in selected_perks {
        println!("Random perk #{}:  {}", ix + 1, perk);
        ix += 1;
    }

    let mut line = String::new();
    std::io::stdin().read_line(&mut line).ok();
    
}
